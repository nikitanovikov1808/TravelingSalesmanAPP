//
//  StartCollectionViewCell.swift
//  TravelingSalesmanAPP
//
//  Created by Nikita Novikov on 15.11.17.
//  Copyright © 2017 Nikita Novikov. All rights reserved.
//

import UIKit

class StartCollectionViewCell: UICollectionViewCell {
    
	@IBOutlet weak var textLabel: UILabel!
	@IBOutlet weak var imageView: UIImageView!
	
}
