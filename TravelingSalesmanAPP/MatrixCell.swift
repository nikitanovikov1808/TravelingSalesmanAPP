//
//  MatrixCell.swift
//  TravelingSalesmanAPP
//
//  Created by Nikita Novikov on 10.11.17.
//  Copyright © 2017 Nikita Novikov. All rights reserved.
//

import UIKit

class MatrixCell: UICollectionViewCell {
    
	@IBOutlet weak var label: UILabel!
}
