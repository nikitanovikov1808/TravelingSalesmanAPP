//
//  City.swift
//  TravelingSalesmanAPP
//
//  Created by Nikita Novikov on 10.11.17.
//  Copyright © 2017 Nikita Novikov. All rights reserved.
//

import UIKit

class City: NSObject {
	let name : String
	var info =  [String: Int]()
	init (name: String) {
		self.name = name
	}
}
