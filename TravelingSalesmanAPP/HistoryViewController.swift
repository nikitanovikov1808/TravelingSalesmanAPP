//
//  HistoryViewController.swift
//  TravelingSalesmanAPP
//
//  Created by Nikita Novikov on 14.11.17.
//  Copyright © 2017 Nikita Novikov. All rights reserved.
//

import UIKit
import CoreData

let tableViewIdentifier = "tableViewCell"

class HistoryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

	var arrayFromOneCityToOtherLengths = [Array<Any>]()
	var arrayFromOneCityToOtherNames = [Array<Any>]()
	var arrayDataSourcesLenghts = [Array<Any>]()
	var dates = [Date]()
	
	var matrixDataSource : MatrixDataSource?
	
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var matrix: UICollectionView!
	@IBOutlet weak var resultLabel: UILabel!
	
	
	override func viewDidLoad() {
        super.viewDidLoad()
		self.navigationItem.title = "История путешествий"
		
		self.tableView.dataSource = self
		self.tableView.delegate = self
		
		fetchResultFromDB()

    }
	
	
	// MARK:- CoreData

	func fetchResultFromDB() {
		let appDelegate = UIApplication.shared.delegate as! AppDelegate
		let managedContext = appDelegate.persistentContainer.viewContext
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Decision")
		
		do {
			let results = try managedContext.fetch(fetchRequest)
			if results.count != 0 {
				for result in results {
					let dataSourceLenghts = (result as! NSManagedObject).value(forKey: "dataSourceLenghts") as! NSData
					let unarchiveObjDataSourceLenghts = NSKeyedUnarchiver.unarchiveObject(with: dataSourceLenghts as Data)
					self.arrayDataSourcesLenghts.append(unarchiveObjDataSourceLenghts as AnyObject! as! [Int])
					
					let dataFromOneCityToOtherLengths = (result as! NSManagedObject).value(forKey: "fromOneCityToOtherLengths") as! NSData
					let unarchiveObjDataFromOneCityToOtherLenghts = NSKeyedUnarchiver.unarchiveObject(with: dataFromOneCityToOtherLengths as Data)
					self.arrayFromOneCityToOtherLengths.append(unarchiveObjDataFromOneCityToOtherLenghts as AnyObject! as! [Int])
					
					let dataFromOneCityToOtherNames = (result as! NSManagedObject).value(forKey: "fromOneCityToOtherNames") as! NSData
					let unarchiveObjDataFromOneCityToOtherNames = NSKeyedUnarchiver.unarchiveObject(with: dataFromOneCityToOtherNames as Data)
					self.arrayFromOneCityToOtherNames.append(unarchiveObjDataFromOneCityToOtherNames as AnyObject! as! [String])
					
					let date = (result as! NSManagedObject).value(forKey: "date") as! Date
					self.dates.append(date)
				}
			}
			
		} catch let error as NSError {
			print("CoreData could not fetch \(error), \(error.userInfo)")
		}
		
		if self.dates.count == 0 {
			let alert = UIAlertController(title: "История путешествий пуста", message: "Отправьте коммивояжера в новое путешествие, чтобы история заполнилась", preferredStyle: UIAlertControllerStyle.alert)
			alert.addAction(UIAlertAction(title: "Отправить", style: UIAlertActionStyle.default, handler: { action in
				self.navigationController?.popViewController(animated: true)
			}))
			self.present(alert, animated: true, completion: nil)
		}
		
		else if (self.arrayFromOneCityToOtherNames.count != self.arrayFromOneCityToOtherLengths.count
			||  self.arrayDataSourcesLenghts.count != self.arrayFromOneCityToOtherLengths.count
			||  self.arrayDataSourcesLenghts.count != self.dates.count) {
			let alert = UIAlertController(title: "Ошибка", message: "Ошибка получения данных из базы данных", preferredStyle: UIAlertControllerStyle.alert)
			alert.addAction(UIAlertAction(title: "Назад", style: UIAlertActionStyle.default, handler: { action in
				self.navigationController?.popViewController(animated: true)
			}))
			alert.addAction(UIAlertAction(title: "Повторить", style: UIAlertActionStyle.default, handler: { action in
				self.fetchResultFromDB()
			}))
			self.present(alert, animated: true, completion: nil)
		}
		else {
			self.tableView.reloadData()
		}
	}

	
	func showResult(withIndex : Int) {
		var result = ""
		var citySet =  Set<String>()
		citySet.insert("")
		for city in arrayFromOneCityToOtherNames[withIndex] as! [String] {
			if (!citySet.contains(city)) {
				if (citySet.count > 1) {
					result.append("->")
				}
				result.append(city)
				citySet.insert(city)
			}
		}
		result.append("\n")
		for length in arrayFromOneCityToOtherLengths[withIndex] as! [Int] {
			result.append("--")
			if (length == 999) {
				break;
			}
			result.append(String(length))
		}
		self.resultLabel.text = result
	}
	
	// MARK:- UITableView DataSource
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return arrayFromOneCityToOtherLengths.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: tableViewIdentifier, for: indexPath)
		
		if indexPath.row < self.dates.count {
			let formatter = DateFormatter()
			formatter.timeStyle = .short
			formatter.dateStyle = .short
			cell.textLabel?.text = formatter.string(from: self.dates[(arrayFromOneCityToOtherLengths.count - 1) - indexPath.row])
		}
		
		if indexPath.row < self.arrayFromOneCityToOtherNames.count {
			var detailText = ""
			var namesSet =  Set<String>()
			let names = self.arrayFromOneCityToOtherNames[(arrayFromOneCityToOtherLengths.count - 1) - indexPath.row] as! Array<String>
			for name in names {
				if name == "" {
					break
				}
				if (!namesSet.contains(name)) {
					if names.index(of: name) != 0 {
						detailText.append("->")
					}
					detailText.append(name)
					namesSet.insert(name)
				}
			}
			cell.detailTextLabel?.text = detailText
		}
		return cell
	}
	
	
	// MARK:- UITableView Delegate
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		self.matrixDataSource = MatrixDataSource(fromOneCityToOtherLengths: self.arrayFromOneCityToOtherLengths[(arrayFromOneCityToOtherLengths.count - 1) - indexPath.row] as! [Int], fromOneCityToOtherNames: self.arrayFromOneCityToOtherNames[(arrayFromOneCityToOtherLengths.count - 1) - indexPath.row] as! [String], dataSourceLenghts: self.arrayDataSourcesLenghts[(arrayFromOneCityToOtherLengths.count - 1) - indexPath.row] as! [Int])
		
		self.matrix.dataSource = self.matrixDataSource
		self.matrix.delegate = self.matrixDataSource
		
		self.matrix.reloadData()
		showResult(withIndex: indexPath.row)
	}
}
