//
//  StartViewController.swift
//  TravelingSalesmanAPP
//
//  Created by Nikita Novikov on 15.11.17.
//  Copyright © 2017 Nikita Novikov. All rights reserved.
//

import UIKit

let startCellIdentifier = "startCell"

class StartViewController: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource {

	@IBOutlet weak var collectionView: UICollectionView!
	@IBOutlet weak var pageControl: UIPageControl!
	@IBOutlet weak var continueButton: UIButton!
	
	var texts = [String]()
	var selectedIndex = 0 {
		willSet(value) {
			self.pageControl.currentPage = value
			UIView.animate(withDuration: 1.0, animations: { () -> Void in
				self.view.backgroundColor = self.randomColor()})
			if (value == 5) {
				self.continueButton.backgroundColor = UIColor.red
			}
		}
	}
	
	override func viewDidLoad() {
        super.viewDidLoad()

		configureUI()
		provideText()

    }

	
	func configureUI() {
		self.view.backgroundColor = randomColor()
		
		self.collectionView.delegate = self
		self.collectionView.dataSource = self
		self.collectionView.decelerationRate = UIScrollViewDecelerationRateFast;
		self.collectionView.isUserInteractionEnabled = true;
		self.collectionView.isScrollEnabled = true;
		self.collectionView.clipsToBounds = true;
		self.collectionView.showsVerticalScrollIndicator = false;
		self.collectionView.showsHorizontalScrollIndicator = false;
		
		
		self.continueButton.layer.cornerRadius = 12
		self.continueButton.clipsToBounds = false
		self.continueButton.layer.shadowColor = UIColor.black.cgColor
		self.continueButton.layer.shadowOpacity = 0.3
		self.continueButton.layer.shadowOffset = CGSize(width: 0, height: 10)
		self.continueButton.layer.shadowPath = UIBezierPath(roundedRect: self.continueButton.bounds, cornerRadius: 12).cgPath
		self.continueButton.layer.shadowRadius = 5
	}

		
	func provideText() {
		let text1 = """
					Привет!
					Данное приложение решает
					задачу коммивояжера
					"""
		
		let text2 = """
					Задача коммивояжёра — одна из самых известных задач
					комбинаторной оптимизации,
					заключающаяся в поиске самого
					выгодного маршрута, проходящего
					через указанные города хотя бы
					по одному разу с последующим
					возвратом в исходный город
					"""
		
		let text3 = """
					Приложение решает эту задачу
					с помощью жадного алгоритма,
					суть которого  в принятии локально
					оптимальных решений на каждом этапе
					"""
		
		let text4 = """
					Все что вам нужно - ввести количество городов,
					в которых должен побывать коммивояжер. Алгоритм
					случайно выбирает город, из которого
					стартует коммивояжер и находит
					наиболее выгодный маршрут
					"""
		
		let text5 = """
					Приложение показывает матрицу расстояний
					между городами и отмечает на ней
					кратчайший путь с помощью
					градиентной подсветки!
					Все предыдущие результаты сохраняются и их можно воспроизвести
					"""
		
		let text6 = """
					Меньше слов - больше дела!
					В путь!
					"""
		
		self.texts.append(text1)
		self.texts.append(text2)
		self.texts.append(text3)
		self.texts.append(text4)
		self.texts.append(text5)
		self.texts.append(text6)
	}
	
	
	// MARK:- UICollectionView DataSource
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return self.texts.count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: startCellIdentifier, for: indexPath) as! StartCollectionViewCell
		cell.textLabel.text = self.texts[indexPath.row]
		cell.imageView.image = UIImage(named: String(indexPath.row + 1))!
		cell.layer.cornerRadius = 30
		cell.clipsToBounds = false
		cell.layer.shadowColor = UIColor.black.cgColor
		cell.layer.shadowOpacity = 0.3
		cell.layer.shadowOffset = CGSize(width: 0, height: 10)
		cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: 30).cgPath
		cell.layer.shadowRadius = 5

		return cell
	}
	
	// MARK:- UICollectionView Delegate
	
	func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {

		let nearestPage = rintf(Float(targetContentOffset.pointee.x) / (460 + 40))
		self.selectedIndex = Int(nearestPage)
		
		targetContentOffset.pointee.x = CGFloat(nearestPage * (460 + 40))

	}
	
	
	// MARK:- Helpers

	func randomColor() -> UIColor {
		
		let randomRed:CGFloat = CGFloat(arc4random_uniform(256))
		let randomGreen:CGFloat = CGFloat(arc4random_uniform(256))
		let randomBlue:CGFloat = CGFloat(arc4random_uniform(256))
		return UIColor(red: randomRed/255, green: randomGreen/255, blue: randomBlue/255, alpha: 1)
	}

}
