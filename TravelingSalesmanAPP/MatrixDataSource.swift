//
//  MatrixDataSource.swift
//  TravelingSalesmanAPP
//
//  Created by Nikita Novikov on 15.11.17.
//  Copyright © 2017 Nikita Novikov. All rights reserved.
//

import UIKit

let martixCellIdentifier = "martixCell"


class MatrixDataSource: NSObject, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

	var fromOneCityToOtherLengths = [Int]()
	var fromOneCityToOtherNames = [String]()
	var dataSourceLenghts = [Int]()
	
	var cities = [String]()

	var currentColor = UIColor.red
	
	 init(fromOneCityToOtherLengths: [Int], fromOneCityToOtherNames: [String], dataSourceLenghts: [Int] ) {
		self.fromOneCityToOtherNames = fromOneCityToOtherNames
		self.fromOneCityToOtherLengths = fromOneCityToOtherLengths
		self.dataSourceLenghts = dataSourceLenghts
		
		let allCities = ["A","B","C","D","E","F","G","H","I","J"]
		self.cities = Array(allCities[0...fromOneCityToOtherLengths.count-1])
		
		super.init()
	
		self.currentColor = randomColor()
	}
	
	
	// MARK:- UICollectionView DelegateFlowLayout
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		let size = (collectionView.frame.width / CGFloat(self.fromOneCityToOtherLengths.count)) - 10
		return CGSize(width: size, height: size);
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
		return 0
	}
	
	
	// MARK:- UICollectionView DataSource
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return self.fromOneCityToOtherLengths.count*self.fromOneCityToOtherLengths.count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: martixCellIdentifier, for: indexPath) as! MatrixCell
		if indexPath.row < self.dataSourceLenghts.count {
			cell.label.text = String(self.dataSourceLenghts[indexPath.row])
		}
		cell.alpha = 1
		cell.backgroundColor = UIColor.white
		for lenght in self.fromOneCityToOtherLengths {  //выставляем градиент
			if self.dataSourceLenghts[indexPath.row] == lenght {
				if fromOneCityToOtherNames[fromOneCityToOtherLengths.index(of: lenght)! * 2] == self.cities[(indexPath.row / self.cities.count)] {
					cell.backgroundColor = self.currentColor
					cell.alpha =  (CGFloat(fromOneCityToOtherLengths.index(of: lenght)!)) / (CGFloat(self.fromOneCityToOtherLengths.count)) + 0.2
				}
			}
		}
		return cell
	}
	
	
	// MARK:- Helpers
	
	func randomColor() -> UIColor {
		
		let randomRed:CGFloat = CGFloat(arc4random_uniform(256))
		let randomGreen:CGFloat = CGFloat(arc4random_uniform(256))
		let randomBlue:CGFloat = CGFloat(arc4random_uniform(256))
		return UIColor(red: randomRed/255, green: randomGreen/255, blue: randomBlue/255, alpha: 1.0)
	}
	
}
