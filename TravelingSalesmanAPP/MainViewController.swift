//
//  ViewController.swift
//  TravelingSalesmanAPP
//
//  Created by Nikita Novikov on 10.11.17.
//  Copyright © 2017 Nikita Novikov. All rights reserved.
//

import UIKit
import CoreData


class MainViewController: UIViewController {

	let allCities = ["A","B","C","D","E","F","G","H","I","J"]

	@IBOutlet weak var numberOfCitiesTextField: UITextField!
	@IBOutlet weak var matrix: UICollectionView!
	@IBOutlet weak var resultLabel: UILabel!
	
	var cities = [City]()
	
	var fromOneCityToOtherLengths = [Int]()
	var fromOneCityToOtherNames = [String]()
	var dataSourceLenghts = [Int]()
	
	var matrixDataSource : MatrixDataSource?


	// MARK:- Actions
	
	@IBAction func Calculate(_ sender: UIButton) {
		//проверка на валидность введенного количества городов
		let numberOfCities = self.validateInputParams()
		if numberOfCities == -1 {
			return
		}
		self.cities.removeAll()
		self.fromOneCityToOtherLengths.removeAll()
		self.fromOneCityToOtherNames.removeAll()
		self.dataSourceLenghts.removeAll()
		
		//составляем список городов
		generateLenghtsBetweenCities(numberOfCities: numberOfCities)
		
		//решаем задачу коммивояжера с помощью жадного алгоритма
		travelingSalesmanDecision()
		
		makeDataSource()
		configurateMatrix()
		showResult()
		
		saveResultToDB()
	}
	
	
	// MARK:- Algorithm decision
	
	func validateInputParams() -> Int {
		if self.numberOfCitiesTextField.text != nil {
			if let number = Int(self.numberOfCitiesTextField.text!) {
				if  !(number < 11 && number > 3) {
					self.numberOfCitiesTextField.placeholder = "Введите число от 4 до 10"
					self.numberOfCitiesTextField.text = nil
				}
				else {
					self.numberOfCitiesTextField.resignFirstResponder()
					return number
				}
			}
			else {
				self.numberOfCitiesTextField.placeholder = "Введите число от 4 до 10"
				self.numberOfCitiesTextField.text = nil
			}
		}
		return -1
	}
	
	
	func generateLenghtsBetweenCities(numberOfCities: Int) {
		let cities : [String] = Array(allCities[0...numberOfCities-1])
		var uniqueLengths =  Set<Int>()
		
		//генерируем расстояния между городами случайным образом
		for cityname in cities {
			let city = City(name: cityname)
			
			var otherCitiesNames = cities
			otherCitiesNames.remove(at: cities.index(of: cityname)!)  //массив городов без одного текущего по циклу города
			
			for otherCityName:String in otherCitiesNames {   //перебираем каждый город без текущего и генерируем случайное раастояние
				if cities.index(of: cityname)! < cities.index(of: otherCityName)! {  //при этом смотрим если уже есть расстояние из А в В равное 30 то делаем из B в A расстояние также равное 30
					while (true) {
						let uniqueLength = Int(arc4random()%100) + 1  //генерируем уникальные случайные расстояния между городами
						if !uniqueLengths.contains(uniqueLength) {
							uniqueLengths.insert(uniqueLength)
							city.info[otherCityName] = uniqueLength
							break
						}
					}
				}
				else //вот здесь делаем : А -> В = 30 -->>  B -> A = 30
				{
					let infoCity = self.cities[otherCitiesNames.index(of: otherCityName)!]
					city.info[infoCity.name] = infoCity.info[cityname]
				}
			}
			self.cities.append(city)
		}
	}
	
	
	func travelingSalesmanDecision() {
		var cityWhereSalesmanHaveBeen =  Set<String>()
		var nextCityCount = -1
		for _ in 0...self.cities.count {
			if nextCityCount == -1 {  //в первый раз случайно выбираем город из которого стартует коммивояжер
				nextCityCount = Int(arc4random())%self.cities.count
				cityWhereSalesmanHaveBeen.insert(self.cities[nextCityCount].name)
			}
			if (nextCityCount == -2 ) { //флаг выхода
				break
			}
			else {
				let otherCity = self.cities[nextCityCount]  //выбираем город откуда пойдем коммивояжер
				fromOneCityToOtherNames.append(otherCity.name)
				
				var minLength = 999
				var nextCityName = ""
				for (cityName,lengthBetweenCities) in otherCity.info {     // для этого города находим кратчайшее расстояние до другого города и запоминаем параметры
					if (!cityWhereSalesmanHaveBeen.contains(cityName)) {   //в город,в котором уже побывали, больше ходить не надо
						if (lengthBetweenCities < minLength) {
							minLength = lengthBetweenCities;
							nextCityName = cityName;
						}
					}
				}
				cityWhereSalesmanHaveBeen.insert(nextCityName)
				
				for city in self.cities {  //находим город куда решил идти коммивояжер
					if city.name == nextCityName && city.info[otherCity.name] == minLength {
						nextCityCount = self.cities.index(of: city)!  //и находим индекс этого города, чтоб в след итерации стартовать отсюда
						break
					}
					else {    //иначи коммивояжер обошел все города
						nextCityCount = -2
					}
				}
				fromOneCityToOtherNames.append(nextCityName)  
				fromOneCityToOtherLengths.append(minLength)
			}
		}
	}
	
	
	func makeDataSource () {
		let cities : [String] = Array(allCities[0...self.cities.count-1])
		for i in 0...self.cities.count-1 {
			for city in cities {
				if city == self.cities[i].name {
					dataSourceLenghts.append(0)
				}
				else {
					dataSourceLenghts.append(self.cities[i].info[city]!)
				}
			}
		}
	}
	
	
	// MARK:- config UI

	func showResult () {
		var result = ""
		var citySet =  Set<String>()
		citySet.insert("")
		for city in fromOneCityToOtherNames {
			if (!citySet.contains(city)) {
				if (citySet.count > 1) {
					result.append("->")
				}
				result.append(city)
				citySet.insert(city)
			}
		}
		result.append("\n")
		for length in fromOneCityToOtherLengths {
			result.append("--")
			if (length == 999) {
				break;
			}
			result.append(String(length))
		}
		self.resultLabel.text = result
	}
	
	
	func configurateMatrix() {
		self.matrixDataSource = MatrixDataSource(fromOneCityToOtherLengths: self.fromOneCityToOtherLengths, fromOneCityToOtherNames: self.fromOneCityToOtherNames, dataSourceLenghts: self.dataSourceLenghts)
		matrix.dataSource = matrixDataSource
		matrix.delegate = matrixDataSource
		self.matrix.reloadData()
	}
	
	
	// MARK:- CoreData
	
	func saveResultToDB () {
		let appDelegate = UIApplication.shared.delegate as! AppDelegate
		let managedContext = appDelegate.persistentContainer.viewContext
	
		let entity =  NSEntityDescription.entity(forEntityName: "Decision",in: managedContext)
		let decision = NSManagedObject(entity: entity!,
									 insertInto: managedContext)
		
		let dataFromOneCityToOtherLengths = NSKeyedArchiver.archivedData(withRootObject: self.fromOneCityToOtherLengths)
		decision.setValue(dataFromOneCityToOtherLengths, forKey: "fromOneCityToOtherLengths")
		
		let dataFromOneCityToOtherNames = NSKeyedArchiver.archivedData(withRootObject: self.fromOneCityToOtherNames)
		decision.setValue(dataFromOneCityToOtherNames, forKey: "fromOneCityToOtherNames")
		
		let dataSourceLenghts = NSKeyedArchiver.archivedData(withRootObject: self.dataSourceLenghts)
		decision.setValue(dataSourceLenghts, forKey: "dataSourceLenghts")
		
		decision.setValue(Date(), forKey: "date")
		do {
			try managedContext.save()
			print("CoreData save data")
		} catch let error as NSError  {
			print("CoreData could not save data \(error), \(error.userInfo)")
		}
	}
	
}
